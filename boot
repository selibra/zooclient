#!/usr/bin/env php
<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR | E_STRICT);
require __DIR__ . '/vendor/autoload.php';

use Selibra\Bootstrap\Bootstrap;
date_default_timezone_set('PRC');
ini_set("memory_limit", '-1');
ini_set('display_errors', 'On');
\Selibra\Tools\Console::logNF("Start the system.");

try {
    $bootstrap = new Bootstrap();
    $bootstrap->run();
} catch (Exception $e) {
    print_r($e);
}
