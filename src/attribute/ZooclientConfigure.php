<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Zooclient\attribute;


use Selibra\Di\AnnotationConfigure;
use Selibra\Di\AnnotationExecEntity;
use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;
use Selibra\Di\DI;
use Zooclient\context\ZooConfigContext;
use Zooclient\exception\ConfigureException;
use Zooclient\storage\SwooleTableZooStorage;
use Zooclient\ZooclientConfigureInterface;
use Zooclient\ZookeeperClient;

#[\Attribute(\Attribute::TARGET_CLASS)]
class ZooclientConfigure implements SelibraAnnotationInterface
{

    public static ?string $uConfigureClass = null;


    /**
     * @param AnnotationExecEntity $annotationExecEntity
     * @return void
     */
    public function exec(AnnotationExecEntity &$annotationExecEntity)
    {
        self::$uConfigureClass = $annotationExecEntity->getClass();
    }

    public function configure(AnnotationConfigure $annotationConfigure)
    {
        // TODO: Implement configure() method.
    }
}
