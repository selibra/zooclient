FROM registry.cn-hangzhou.aliyuncs.com/selibra/selibra:1.3.8
MAINTAINER 357397264@qq.com


RUN wget https://dlcdn.apache.org/zookeeper/zookeeper-3.5.9/apache-zookeeper-3.5.9.tar.gz \
    && tar zxvf apache-zookeeper-3.5.9.tar.gz

RUN apt-get install -y autotools-dev m4 autoconf2.13 autoconf-archive gnu-standards autoconf-doc libtool

ADD ./install/jdk-8u301-linux-x64.tar.gz /usr/local/java

RUN ln -s /usr/local/java/jdk1.8.0_301 /usr/local/java/jdk

ENV JAVA_HOME /usr/local/java/jdk
ENV JRE_HOME ${JAVA_HOME}/jre
ENV CLASSPATH .:${JAVA_HOME}/lib:${JRE_HOME}/lib
ENV PATH ${JAVA_HOME}/bin:$PATH

ADD ./install/apache-ant-1.10.11 /usr/local/apache-ant-1.10.11

RUN ln -s /usr/local/apache-ant-1.10.11 /usr/local/ant
ENV ANT_HOME /usr/local/ant
ENV PATH=$JAVA_HOME/bin:$PATH:$ANT_HOME/bin

RUN cd apache-zookeeper-3.5.9 && ant compile_jute

RUN cd apache-zookeeper-3.5.9/zookeeper-client/zookeeper-client-c  \
    && autoreconf -if && ACLOCAL="aclocal -I /usr/local/share/aclocal" autoreconf -if \
    && ./configure --without-cppunit --prefix=/usr/apache-zookeeper && make && make install \
    && wget https://pecl.php.net/get/zookeeper-1.0.0.tgz \
    && tar zxvf zookeeper-1.0.0.tgz \
    && cd zookeeper-1.0.0 \
    && phpize \
    && ./configure --with-libzookeeper-dir=/usr/apache-zookeeper && make && make install \
    && cd /usr/local/etc/php/ \
    && echo 'extension=/usr/local/lib/php/extensions/no-debug-non-zts-20200930/zookeeper.so' >> php.ini
