<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Zooclient\listener;

use Selibra\Di\Annotations\Component;
use Selibra\Di\DI;
use Selibra\Event\Annotations\Listener;
use Selibra\Http\Events\HttpStartBeforeEvent;
use Zooclient\attribute\ZooclientConfigure;
use Zooclient\context\ZooConfigContext;
use Zooclient\exception\ConfigureException;
use Zooclient\storage\SwooleTableZooStorage;
use Zooclient\ZooclientConfigureInterface;
use Zooclient\ZookeeperClient;

#[Component]
class OnHttpStartBefore
{

    /**
     * @throws ConfigureException
     */
    #[Listener(HttpStartBeforeEvent::class)]
    public function main()
    {
        $uConfigureClass = ZooclientConfigure::$uConfigureClass;
        if (!empty($uConfigureClass)) {
            $object = DI::getObjectContext(ZooclientConfigure::$uConfigureClass);
            if (!$object instanceof ZooclientConfigureInterface) {
                throw new ConfigureException("ZooclientConfigure class not instanceof ZooclientConfigureInterface.");
            }
            $zooConfigContext = $object->configure(new ZooConfigContext());
            $storage = $zooConfigContext->getStorage();
        }
        if (empty($storage)) {
            $storage = DI::getObjectContext(SwooleTableZooStorage::class);
        }
        $storage->configure();
        ZookeeperClient::setDataStorage($storage);
    }

}
