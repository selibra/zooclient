<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Zooclient\storage;

use Selibra\Config\Config;
use Selibra\Event\Annotations\Listener;
use Selibra\Http\Events\HttpStartBeforeEvent;
use Selibra\Table\TableFactory;
use Swoole\Table;

interface ZooDataStorageInterface
{

    /**
     * 配置
     */
    public function configure();


    /**
     * 读取配置
     * @param string $key
     * @return string|false
     */
    public function read(string $key): string|false;


    /**
     * 写入配置
     * @param string $key
     * @param string $value
     * @return mixed
     */
    public function write(string $key,string $value): bool;

}
