<?php

namespace Example;

use Selibra\Di\Annotations\Component;
use Selibra\Di\DollarPhraseMapper\DollarPhrase;
use Selibra\Http\Annotations\Controller;
use Selibra\Http\Annotations\Route;
use Selibra\Http\Lib\HttpController;
use Selibra\Tools\Console;
use Swoole\Coroutine;
use Zooclient\ZooDollarPhrase;

/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */
#[Component, Controller]
class ZooTestController extends HttpController
{

    #[Route("/test")]
    public function main()
    {
        return $this->success("s", [
            'value' => DollarPhrase::resolve('$zoo.configs.cc')
        ]);
    }

}
